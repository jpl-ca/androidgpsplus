package tracker.jmt.gpsplus.plus;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import tracker.jmt.gpsplus.CalibrationActivity;
import tracker.jmt.gpsplus.DaysActivity;
import tracker.jmt.gpsplus.MainApplication;
import tracker.jmt.gpsplus.MainService;
import tracker.jmt.gpsplus.R;
import tracker.jmt.gpsplus.SettingsActivity;

/**
 * Created by JMTech-Android on 08/06/2015.
 */
public class MainPlusActivity extends ActionBarActivity implements OnMapReadyCallback{
    private GoogleMap map;
    Toolbar toolbar;
    LatLng ME;
    GpsReceiver myReceiver;
    PolylineOptions optionsLine;
    Polyline lineMe;
    Marker markerME;
    private CharSequence mTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plus_map);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("(Y)");
        mTitle = getTitle();
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        optionsLine = new PolylineOptions().width(4).color(Color.parseColor("#e74c3c")).geodesic(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        myReceiver = new GpsReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MainService.NEW_GPS);
        registerReceiver(myReceiver, intentFilter);
    }
    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.onPreferenceChange();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Only show items in the action bar relevant to this screen
        // if the drawer is not showing. Otherwise, let the drawer
        // decide what to show in the action bar.
        getMenuInflater().inflate(R.menu.when_moving, menu);
        //restoreActionBar();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        Intent intent;
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                showLatest();
                return true;
            case R.id.menu_settings:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.show_days:
//                intent = new Intent(this, DaysActivity.class);
                intent = new Intent(this, FusedMainActivity.class);
                startActivityForResult(intent, 1);
                return true;
            case R.id.menu_calibrate:
                intent = new Intent(this, CalibrationActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    protected void showLatest() {
        GregorianCalendar cal;
        String q, dateStart, dateEnd;
        long millisStart, millisEnd;
        cal = new GregorianCalendar(); // local
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        millisStart = cal.getTimeInMillis();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        millisEnd = cal.getTimeInMillis();
        // Now get year, month and day for start and stop in GMT
        cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"), Locale.US); // GMT,
        cal.setTimeInMillis(millisStart);
        dateStart = DateFormat.format("yyyyMMdd", cal).toString();
        cal.setTimeInMillis(millisEnd);
        dateEnd = DateFormat.format("yyyyMMdd", cal).toString();
        q = String.format(
                "day between %s and %s and milliseconds between %d and %d and provider='gps' and accuracy < 40.00",
                dateStart,
                dateEnd,
                millisStart,
                millisEnd
        );
        //showMarkers(q);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setAllGesturesEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        ME = new LatLng(-12.089555406684041,-77.01358316161633);
        MarkerOptions mo = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_place)).position(ME);
        markerME = map.addMarker(mo);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(ME, 12));
        map.animateCamera(CameraUpdateFactory.zoomTo(12), 500, null);
    }


    private class GpsReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            System.out.println("That Emotion");
            Bundle extra = arg1.getExtras();
            if(extra!=null){
                Double Lat = extra.getDouble("latitude");
                Double Lng = extra.getDouble("longitude");
                float accuracy = extra.getFloat("accuracy");
                String provider = extra.getString("provider");
                String tit = Lat+","+Lng;
                ((TextView)findViewById(R.id.txtLL)).setText(tit);
                ((TextView)findViewById(R.id.txtLL3)).setText(accuracy+"  -  "+provider);
                ME =  new LatLng(Lat,Lng);
                if(markerME!=null) markerME.remove();
                MarkerOptions mo = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_place)).position(ME);
                markerME = map.addMarker(mo);
                if(lineMe!=null) lineMe.remove();
                optionsLine.add(ME);
                lineMe = map.addPolyline(optionsLine);
            }
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(myReceiver);
    }
}
